package net.tensberg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabHelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabHelloworldApplication.class, args);
	}
}
